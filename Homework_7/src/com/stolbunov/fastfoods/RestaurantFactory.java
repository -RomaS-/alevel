package com.stolbunov.fastfoods;

import com.stolbunov.fastfoods.burgerclub.BurgerClubFactory;
import com.stolbunov.fastfoods.mcdonalds.McDonaldsFactory;

public class RestaurantFactory {
    public BurgerClubFactory burgerClubFactory;
    public McDonaldsFactory mcDonaldsFactory;

    public RestaurantFactory() {
        burgerClubFactory = new BurgerClubFactory();
        mcDonaldsFactory = new McDonaldsFactory();
    }

    public IRestaurant createRestaurant(RestaurantType type) {
        switch (type) {
            case MCDONALDS:
                return mcDonaldsFactory.createNewMcDonalds();
            case BURGERCLUB:
                return burgerClubFactory.createNewBurgerClub();
        }
        throw new RestaurantNotDefined();
    }


    public enum RestaurantType {
        MCDONALDS, BURGERCLUB
    }

    public class RestaurantNotDefined extends RuntimeException {

    }
}
