package com.stolbunov.fastfoods.workflow_restaurants.table_order;

import com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception.order_exception.OrderException;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;

public interface IChoiceKitchen {
    ICookFood choice(String nameOrder) throws OrderException;
}
