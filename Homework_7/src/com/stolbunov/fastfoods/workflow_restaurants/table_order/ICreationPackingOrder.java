package com.stolbunov.fastfoods.workflow_restaurants.table_order;

public interface ICreationPackingOrder {
    IPackingOrder create();
}
