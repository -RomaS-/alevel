package com.stolbunov.fastfoods.workflow_restaurants.table_order;

import java.util.List;

public interface IOrderTable {
    IPackingOrder makeOrder(List<String> orders);
}
