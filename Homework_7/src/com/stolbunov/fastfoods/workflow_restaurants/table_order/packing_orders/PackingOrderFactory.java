package com.stolbunov.fastfoods.workflow_restaurants.table_order.packing_orders;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.ICreationPackingOrder;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.IPackingOrder;

public class PackingOrderFactory implements ICreationPackingOrder {
    @Override
    public IPackingOrder create() {
        return new PackingOrder();
    }
}
