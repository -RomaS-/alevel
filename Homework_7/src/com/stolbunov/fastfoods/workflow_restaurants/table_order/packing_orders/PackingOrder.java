package com.stolbunov.fastfoods.workflow_restaurants.table_order.packing_orders;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.IPackingOrder;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;

import java.util.ArrayList;
import java.util.List;

public class PackingOrder implements IPackingOrder {
    private List<ICookFood> cookFoods;

    public PackingOrder() {
        cookFoods = new ArrayList<>();
    }

    @Override
    public void addToKit(ICookFood cookFood) {
        cookFoods.add(cookFood);
    }

    @Override
    public List<ICookFood> getKit() {
        return cookFoods;
    }
}
