package com.stolbunov.fastfoods.workflow_restaurants.table_order;

import com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception.order_exception.OrderException;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ManagerKitchen;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.packing_orders.PackingOrderFactory;

import java.util.List;

public class TableOrder implements IOrderTable {
    private ManagerKitchen managerKitchen;
    private ICreationPackingOrder creationPackingOrder;

    public TableOrder() {
        managerKitchen = new ManagerKitchen();
        creationPackingOrder = new PackingOrderFactory();
    }

    @Override
    public IPackingOrder makeOrder(List<String> orders) {
        IPackingOrder packingOrder = creationPackingOrder.create();
        creationOrder(orders, packingOrder);
        messageOrderIsReady(packingOrder.getKit());
        return packingOrder;
    }


    private void creationOrder(List<String> orders, IPackingOrder packingOrder) {
        for (String order : orders) {
            try {
                packingOrder.addToKit(managerKitchen.choice(order));
            } catch (OrderException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void messageOrderIsReady(List<ICookFood> namesFood) {
        System.out.println("<<Kitchen>> The order is ready: " + namesFood.toString());
    }
}
