package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage;

public interface IRaw {
    String MEAT_MCDONALDS = "Meat_McDonalds";
    String CHEESE_MCDONALDS = "Cheese_McDonalds";
    String POTATO_MCDONALDS = "Potato_McDonalds";
    String BREAD_MCDONALDS = "Bread_McDonalds";
    String MEAT_BURGERCLUB = "Meat_BurgerClub";
    String CHEESE_BURGERCLUB = "Cheese_BurgerClub";
    String POTATO_BURGERCLUB = "Potato_BurgerClub";
    String BREAD_BURGERCLUB = "Bread_BurgerClub";

    int giveRaw(int amountRaw);
}
