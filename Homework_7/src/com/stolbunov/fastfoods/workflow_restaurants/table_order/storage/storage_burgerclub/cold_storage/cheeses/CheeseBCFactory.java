package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_burgerclub.cold_storage.cheeses;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRawFactory;

public class CheeseBCFactory implements IRawFactory {
    @Override
    public IRaw create() {
        return new CheeseBC();
    }
}
