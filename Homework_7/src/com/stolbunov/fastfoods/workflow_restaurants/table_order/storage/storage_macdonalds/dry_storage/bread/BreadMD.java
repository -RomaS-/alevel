package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_macdonalds.dry_storage.bread;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.BaseRaw;

public class BreadMD extends BaseRaw {
    private final int maximumAmount = 100;
    private static int remainder;

    @Override
    public int giveRaw(int amountRaw) {
        if ((remainder += amountRaw) > maximumAmount) {
            messageIfNoRaw();
            replenishment();
        }
        return amountRaw;
    }

    private void replenishment() {
        remainder = 0;
    }
}
