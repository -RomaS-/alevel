package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage;

public interface IRawFactory {
    IRaw create();
}
