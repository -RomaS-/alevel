package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_burgerclub.dry_storage.bread;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRawFactory;

public class BreadBCFactory implements IRawFactory {
    @Override
    public IRaw create() {
        return new BreadBC();
    }
}
