package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_burgerclub.cold_storage.meats;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRawFactory;

public class MeatBCFactory implements IRawFactory {
    @Override
    public IRaw create() {
        return new MeatBC();
    }
}
