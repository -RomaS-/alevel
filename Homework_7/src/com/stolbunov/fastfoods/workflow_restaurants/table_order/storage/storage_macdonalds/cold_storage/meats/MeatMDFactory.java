package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_macdonalds.cold_storage.meats;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRawFactory;

public class MeatMDFactory implements IRawFactory {
    @Override
    public IRaw create() {
        return new MeatMD();
    }
}
