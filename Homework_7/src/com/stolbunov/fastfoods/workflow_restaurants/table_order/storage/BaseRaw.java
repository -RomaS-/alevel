package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage;

public abstract class BaseRaw implements IRaw {

    protected void messageIfNoRaw() {
        System.out.println(
                "<<Storage>> Sorry, raw materials have run out. Wait one minute, please.\n" +
                        "<<Storage>> Replenishment .....\n" +
                        "<<Storage>> Thank you for your expectations.");
    }
}
