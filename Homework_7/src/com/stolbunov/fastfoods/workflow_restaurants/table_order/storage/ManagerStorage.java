package com.stolbunov.fastfoods.workflow_restaurants.table_order.storage;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.IManagerStorage;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_burgerclub.cold_storage.cheeses.CheeseBCFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_burgerclub.cold_storage.meats.MeatBCFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_burgerclub.cold_storage.potatoes.PotatoBCFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_burgerclub.dry_storage.bread.BreadBCFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_macdonalds.cold_storage.cheeses.CheeseMDFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_macdonalds.cold_storage.meats.MeatMDFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_macdonalds.cold_storage.potatoes.PotatoMDFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.storage_macdonalds.dry_storage.bread.BreadMDFactory;

import static com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw.*;

public class ManagerStorage implements IManagerStorage {
    private IRawFactory factoryCheeseMD;
    private IRawFactory factoryMeatMD;
    private IRawFactory factoryPotatoMD;
    private IRawFactory factoryBreadMD;

    private IRawFactory factoryCheeseBC;
    private IRawFactory factoryMeatBC;
    private IRawFactory factoryPotatoBC;
    private IRawFactory factoryBreadBC;

    private static ManagerStorage managerStorage;

    private ManagerStorage() {
        factoryCheeseMD = new CheeseMDFactory();
        factoryMeatMD = new MeatMDFactory();
        factoryPotatoMD = new PotatoMDFactory();
        factoryBreadMD = new BreadMDFactory();

        factoryCheeseBC = new CheeseBCFactory();
        factoryMeatBC = new MeatBCFactory();
        factoryPotatoBC = new PotatoBCFactory();
        factoryBreadBC = new BreadBCFactory();
    }

    public static ManagerStorage getInstance() {
        if (managerStorage == null) {
            managerStorage = new ManagerStorage();
        }
        return managerStorage;
    }

    @Override
    public IRaw getRawForProduction(String nameRaw, int amount) {
        switch (nameRaw) {
            case MEAT_MCDONALDS:
                return getRaw(factoryMeatMD, amount);
            case CHEESE_MCDONALDS:
                return getRaw(factoryCheeseMD, amount);
            case POTATO_MCDONALDS:
                return getRaw(factoryPotatoMD, amount);
            case BREAD_MCDONALDS:
                return getRaw(factoryBreadMD, amount);
            case MEAT_BURGERCLUB:
                return getRaw(factoryMeatBC, amount);
            case CHEESE_BURGERCLUB:
                return getRaw(factoryCheeseBC, amount);
            case POTATO_BURGERCLUB:
                return getRaw(factoryPotatoBC, amount);
            case BREAD_BURGERCLUB:
                return getRaw(factoryBreadBC, amount);
        }
        throw new IllegalArgumentException("Incorrect name of raw materials. Select or add it to Raw");
    }

    private IRaw getRaw(IRawFactory factory, int amount) {
        IRaw iRaw = factory.create();
        iRaw.giveRaw(amount);
        return iRaw;
    }
}
