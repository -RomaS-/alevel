package com.stolbunov.fastfoods.workflow_restaurants.table_order;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;

import java.util.List;

public interface IPackingOrder {
    void addToKit(ICookFood cookFood);

    List<ICookFood> getKit();
}
