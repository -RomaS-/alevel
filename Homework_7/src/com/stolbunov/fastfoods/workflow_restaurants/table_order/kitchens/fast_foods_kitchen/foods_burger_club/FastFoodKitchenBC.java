package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club;

import com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception.order_exception.OrderException;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFoodKitchen;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.ICreationFoodFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.cheeseburgers.CheeseburgerBCFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.chickenburger.ChickenburgerBCFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.french_fries.FrenchFriesBCFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.hamburger.HamburgerBCFactory;

import static com.stolbunov.fastfoods.workflow_restaurants.menu.menu_burger_club.FastFoodMenuBC.*;

public class FastFoodKitchenBC extends BaseFastFoodKitchen {
    private ICreationFoodFactory hamburgerBC;
    private ICreationFoodFactory cheeseburgerBC;
    private ICreationFoodFactory chickenburgerBC;
    private ICreationFoodFactory frenchFriesBC;

    public FastFoodKitchenBC() {
        hamburgerBC = new HamburgerBCFactory();
        cheeseburgerBC = new CheeseburgerBCFactory();
        chickenburgerBC = new ChickenburgerBCFactory();
        frenchFriesBC = new FrenchFriesBCFactory();
    }

    @Override
    public ICookFood cook(String nameFood) {
        switch (nameFood) {
            case HAMBURGER_BC:
                return getCookFood(hamburgerBC, nameFood);
            case CHEESEBURGER_BC:
                return getCookFood(cheeseburgerBC, nameFood);
            case CHICKENBURGER_BC:
                return getCookFood(chickenburgerBC, nameFood);
            case FRENCH_FRIES_BC:
                return getCookFood(frenchFriesBC, nameFood);
        }
        throw new OrderException("<<Kitchen>> Sorry, but this dish is not on the menu_mcdonald!");
    }
}
