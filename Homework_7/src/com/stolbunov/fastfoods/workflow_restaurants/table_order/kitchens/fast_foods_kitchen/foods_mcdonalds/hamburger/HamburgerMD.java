package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.hamburger;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public class HamburgerMD extends BaseFastFood {
    private final int BREAD = 10;
    private final int MEAT = 20;

    @Override
    public void cook() {
        managerStorage.getRawForProduction(IRaw.BREAD_MCDONALDS, BREAD);
        managerStorage.getRawForProduction(IRaw.MEAT_MCDONALDS, MEAT);
    }
}
