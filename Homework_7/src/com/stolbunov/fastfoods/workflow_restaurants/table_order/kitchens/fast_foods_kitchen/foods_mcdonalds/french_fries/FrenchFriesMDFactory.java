package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.french_fries;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.ICreationFoodFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;

public class FrenchFriesMDFactory implements ICreationFoodFactory {

    @Override
    public ICookFood creating() {
        return new FrenchFriesMD();
    }
}
