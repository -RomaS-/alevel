package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.cheeseburgers;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public class CheeseburgerBC extends BaseFastFood {
    private final int BREAD = 30;
    private final int MEAT = 45;
    private final int CHEESE = 40;

    @Override
    public void cook() {
        managerStorage.getRawForProduction(IRaw.BREAD_BURGERCLUB, BREAD);
        managerStorage.getRawForProduction(IRaw.MEAT_BURGERCLUB, MEAT);
        managerStorage.getRawForProduction(IRaw.CHEESE_BURGERCLUB, CHEESE);
    }
}
