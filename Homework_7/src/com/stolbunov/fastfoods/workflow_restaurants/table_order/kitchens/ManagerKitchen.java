package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens;

import com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception.order_exception.OrderException;
import com.stolbunov.fastfoods.workflow_restaurants.menu.IMenuForKitchen;
import com.stolbunov.fastfoods.workflow_restaurants.menu.menu_burger_club.FastFoodMenuBC;
import com.stolbunov.fastfoods.workflow_restaurants.menu.menu_mcdonald.FastFoodMenuMD;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.IChoiceKitchen;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.FastFoodKitchenBC;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.FastFoodKitchenMD;


public class ManagerKitchen implements IChoiceKitchen {
    private IKitchenCook fastFoodKitchenMD;
    private IMenuForKitchen fastFoodMenuMC;
    private IKitchenCook fastFoodKitchenBC;
    private IMenuForKitchen fastFoodMenuBC;


    public ManagerKitchen() {
        fastFoodKitchenMD = new FastFoodKitchenMD();
        fastFoodMenuMC = FastFoodMenuMD.getInstance();
        fastFoodKitchenBC = new FastFoodKitchenBC();
        fastFoodMenuBC = FastFoodMenuBC.getInstance();
    }

    @Override
    public ICookFood choice(String nameOrder) throws OrderException {
        IKitchenCook kitchenCook = choiceOfKitchen(nameOrder);
        return kitchenCook.cook(nameOrder);
    }

    private IKitchenCook choiceOfKitchen(String nameOrder) throws OrderException {
        if (fastFoodMenuMC.getAssortment().contains(nameOrder)) {
            return fastFoodKitchenMD;
        } else if (fastFoodMenuBC.getAssortment().contains(nameOrder)) {
            return fastFoodKitchenBC;
        }
        throw new OrderException("<<Kitchen>> Sorry, but this dish is not on the menu_mcdonald!");
    }
}
