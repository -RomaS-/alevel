package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens;

public interface IKitchenCook {
    ICookFood cook(String nameFood);
}
