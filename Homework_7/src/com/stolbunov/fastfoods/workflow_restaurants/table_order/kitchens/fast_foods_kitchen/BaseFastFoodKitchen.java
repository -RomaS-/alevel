package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.IKitchenCook;

public abstract class BaseFastFoodKitchen implements IKitchenCook {

    protected ICookFood getCookFood(ICreationFoodFactory factory, String nameFood) {
        ICookFood cookFood = factory.creating();
        cookFood.setName(nameFood);
        return cookFood;
    }
}
