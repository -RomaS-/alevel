package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.ManagerStorage;

public abstract class BaseFastFood implements ICookFood {
    private String name;
    protected static ManagerStorage managerStorage;

    public BaseFastFood() {
        managerStorage = ManagerStorage.getInstance();
        cook();
    }

    protected abstract void cook();

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
