package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.chickenburger;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public class ChickenburgerMD extends BaseFastFood {
    private final int BREAD = 10;
    private  final int MEAT = 30;

    @Override
    protected void cook() {
        managerStorage.getRawForProduction(IRaw.BREAD_MCDONALDS, BREAD);
        managerStorage.getRawForProduction(IRaw.MEAT_MCDONALDS, MEAT);
    }
}
