package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds;

import com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception.order_exception.OrderException;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFoodKitchen;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.ICreationFoodFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.cheeseburgers.CheeseburgerMDFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.chickenburger.ChickenburgerMDFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.french_fries.FrenchFriesMDFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.hamburger.HamburgerMDFactory;

import static com.stolbunov.fastfoods.workflow_restaurants.menu.menu_mcdonald.FastFoodMenuMD.*;

public class FastFoodKitchenMD extends BaseFastFoodKitchen {
    private ICreationFoodFactory hamburgerMD;
    private ICreationFoodFactory cheeseburgerMD;
    private ICreationFoodFactory chickenburgerMD;
    private ICreationFoodFactory frenchFriesMD;

    public FastFoodKitchenMD() {
        hamburgerMD = new HamburgerMDFactory();
        cheeseburgerMD = new CheeseburgerMDFactory();
        chickenburgerMD = new ChickenburgerMDFactory();
        frenchFriesMD = new FrenchFriesMDFactory();
    }

    @Override
    public ICookFood cook(String nameFood) throws OrderException {
        switch (nameFood) {
            case HAMBURGER_MD:
                return getCookFood(hamburgerMD, nameFood);
            case CHEESEBURGER_MD:
                return getCookFood(cheeseburgerMD, nameFood);
            case CHICKENBURGER_MD:
                return getCookFood(chickenburgerMD, nameFood);
            case FRENCH_FRIES_MD:
                return getCookFood(frenchFriesMD, nameFood);
        }
        throw new OrderException("<<Kitchen>> Sorry, but this dish is not on the menu_mcdonald!");
    }
}
