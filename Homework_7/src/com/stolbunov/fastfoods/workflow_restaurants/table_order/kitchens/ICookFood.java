package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens;

public interface ICookFood {

    void setName(String name);

    String getName();
}
