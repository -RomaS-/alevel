package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.hamburger;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public class HamburgerBC extends BaseFastFood {
    private final int BREAD = 20;
    private final int MEAT = 30;

    @Override
    public void cook() {
        managerStorage.getRawForProduction(IRaw.BREAD_BURGERCLUB, BREAD);
        managerStorage.getRawForProduction(IRaw.MEAT_BURGERCLUB, MEAT);
    }
}
