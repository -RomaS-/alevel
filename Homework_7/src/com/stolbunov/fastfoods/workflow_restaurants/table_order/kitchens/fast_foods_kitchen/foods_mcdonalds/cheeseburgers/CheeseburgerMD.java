package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.cheeseburgers;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public class CheeseburgerMD extends BaseFastFood {
    private final int BREAD = 20;
    private final int MEAT = 40;
    private final int CHEESE = 30;

    @Override
    public void cook() {
        managerStorage.getRawForProduction(IRaw.BREAD_MCDONALDS, BREAD);
        managerStorage.getRawForProduction(IRaw.MEAT_MCDONALDS, MEAT);
        managerStorage.getRawForProduction(IRaw.CHEESE_MCDONALDS, CHEESE);

    }
}