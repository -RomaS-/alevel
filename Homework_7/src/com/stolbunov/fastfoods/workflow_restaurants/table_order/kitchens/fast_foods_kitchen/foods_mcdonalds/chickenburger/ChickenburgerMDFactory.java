package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.chickenburger;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.ICreationFoodFactory;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;

public class ChickenburgerMDFactory implements ICreationFoodFactory {

    @Override
    public ICookFood creating() {
        return new ChickenburgerMD();
    }
}
