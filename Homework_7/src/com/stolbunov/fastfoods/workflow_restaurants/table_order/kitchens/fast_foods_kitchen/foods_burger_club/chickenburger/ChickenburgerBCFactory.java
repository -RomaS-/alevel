package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.chickenburger;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.ICreationFoodFactory;

public class ChickenburgerBCFactory implements ICreationFoodFactory {
    @Override
    public ICookFood creating() {
        return new ChickenburgerBC();
    }
}
