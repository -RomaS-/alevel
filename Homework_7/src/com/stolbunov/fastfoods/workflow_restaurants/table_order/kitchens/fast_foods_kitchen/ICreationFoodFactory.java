package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;

public interface ICreationFoodFactory {
    public ICookFood creating();
}
