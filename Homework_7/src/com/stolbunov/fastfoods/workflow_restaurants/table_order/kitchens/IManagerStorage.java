package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public interface IManagerStorage {

    IRaw getRawForProduction(String nameRaw, int amount);
}
