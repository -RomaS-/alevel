package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_burger_club.french_fries;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public class FrenchFriesBC extends BaseFastFood {
    private final int POTATO = 30;

    @Override
    protected void cook() {
        managerStorage.getRawForProduction(IRaw.POTATO_BURGERCLUB, POTATO);
    }
}
