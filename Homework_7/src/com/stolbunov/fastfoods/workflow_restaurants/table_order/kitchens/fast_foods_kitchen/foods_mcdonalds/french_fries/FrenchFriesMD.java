package com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.foods_mcdonalds.french_fries;

import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.fast_foods_kitchen.BaseFastFood;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.storage.IRaw;

public class FrenchFriesMD extends BaseFastFood {
    private final int POTATO = 20;

    @Override
    protected void cook() {
        managerStorage.getRawForProduction(IRaw.POTATO_MCDONALDS, POTATO);
    }
}
