package com.stolbunov.fastfoods.workflow_restaurants.menu;

import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.fastfoods.burgerclub.BurgerClub;
import com.stolbunov.fastfoods.mcdonalds.McDonald;
import com.stolbunov.fastfoods.workflow_restaurants.menu.menu_burger_club.FastFoodMenuBC;
import com.stolbunov.fastfoods.workflow_restaurants.menu.menu_mcdonald.FastFoodMenuMD;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Menu implements IRestaurantMenu {
    private static Menu menu;
    private IMenuForKitchen menuMD;
    private IMenuForKitchen menuBC;

    private Menu() {
        menuMD = FastFoodMenuMD.getInstance();
        menuBC = FastFoodMenuBC.getInstance();
    }

    public static Menu getInstance() {
        if (menu == null) {
            menu = new Menu();
        }
        return menu;
    }

    @Override
    public Map<String, Integer> getMenu(IRestaurant restaurant) {
        return getRightMenu(restaurant).getMenu();
    }

    @Override
    public List<String> getFullAssortment(IRestaurant restaurant) {
        return getRightMenu(restaurant).getAssortment();
    }

    @Override
    public Collection<Integer> getPrice(IRestaurant restaurant) {
        return getRightMenu(restaurant).getPrice();
    }

    private IMenuForKitchen getRightMenu(IRestaurant restaurant) {
        switch (restaurant.getName()) {
            case McDonald.RESTAURANT_NAME:
                return menuMD;
            case BurgerClub.RESTAURANT_NAME:
                return menuBC;
        }
        throw new IllegalArgumentException();
    }
}
