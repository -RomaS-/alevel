package com.stolbunov.fastfoods.workflow_restaurants.menu.menu_mcdonald;

import com.stolbunov.fastfoods.workflow_restaurants.menu.IMenuForKitchen;

import java.util.*;

public class FastFoodMenuMD implements IMenuForKitchen {
    public final static String CHEESEBURGER_MD = "CheeseburgerMD";
    public final static String CHICKENBURGER_MD = "ChickenburgerMD";
    public final static String HAMBURGER_MD = "HamburgerMD";
    public final static String FRENCH_FRIES_MD = "French FriesMD";

    private Map<String, Integer> fastFoodMenu;
    private static FastFoodMenuMD menu;

    private FastFoodMenuMD() {
        fastFoodMenu = new HashMap<>();
        fastFoodMenu.put(CHEESEBURGER_MD, 30);
        fastFoodMenu.put(CHICKENBURGER_MD, 35);
        fastFoodMenu.put(HAMBURGER_MD, 20);
        fastFoodMenu.put(FRENCH_FRIES_MD, 15);
    }

    public static FastFoodMenuMD getInstance() {
        if (menu == null) {
            menu = new FastFoodMenuMD();
        }
        return menu;
    }

    @Override
    public Map<String, Integer> getMenu() {
        return fastFoodMenu;
    }

    @Override
    public List<String> getAssortment() {
        return new ArrayList<>(fastFoodMenu.keySet());
    }

    @Override
    public Collection<Integer> getPrice() {
        return fastFoodMenu.values();
    }
}

