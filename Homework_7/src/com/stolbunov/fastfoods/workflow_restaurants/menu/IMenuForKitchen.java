package com.stolbunov.fastfoods.workflow_restaurants.menu;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IMenuForKitchen {
    Map<String, Integer> getMenu();

    List<String> getAssortment();

    Collection<Integer> getPrice();

}
