package com.stolbunov.fastfoods.workflow_restaurants.menu;

import com.stolbunov.fastfoods.IRestaurant;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IRestaurantMenu {
    Map<String, Integer> getMenu(IRestaurant restaurant);

    List<String> getFullAssortment(IRestaurant restaurant);

    Collection<Integer> getPrice(IRestaurant restaurant);
}
