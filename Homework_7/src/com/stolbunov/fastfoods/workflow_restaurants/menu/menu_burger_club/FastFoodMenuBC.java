package com.stolbunov.fastfoods.workflow_restaurants.menu.menu_burger_club;

import com.stolbunov.fastfoods.workflow_restaurants.menu.IMenuForKitchen;

import java.util.*;

public class FastFoodMenuBC implements IMenuForKitchen {

    public final static String CHEESEBURGER_BC = "CheeseburgerBC";
    public final static String CHICKENBURGER_BC = "ChickenburgerBC";
    public final static String HAMBURGER_BC = "HamburgerBC";
    public final static String FRENCH_FRIES_BC = "French Fries BC";

    private Map<String, Integer> fastFoodMenu;
    private static FastFoodMenuBC menu;

    private FastFoodMenuBC() {
        fastFoodMenu = new HashMap<>();
        fastFoodMenu.put(CHEESEBURGER_BC, 45);
        fastFoodMenu.put(CHICKENBURGER_BC, 30);
        fastFoodMenu.put(HAMBURGER_BC, 25);
        fastFoodMenu.put(FRENCH_FRIES_BC, 20);
    }

    public static FastFoodMenuBC getInstance() {
        if (menu == null) {
            menu = new FastFoodMenuBC();
        }
        return menu;
    }

    @Override
    public Map<String, Integer> getMenu() {
        return fastFoodMenu;
    }

    @Override
    public List<String> getAssortment() {
        return new ArrayList<>(fastFoodMenu.keySet());
    }

    @Override
    public Collection<Integer> getPrice() {
        return fastFoodMenu.values();
    }
}
