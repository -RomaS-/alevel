package com.stolbunov.fastfoods.workflow_restaurants.receptions;

import com.stolbunov.fastfoods.Client;
import com.stolbunov.fastfoods.IRestaurant;

public interface IReception {

    void addClientToHall(Client client, IRestaurant restaurant);

    default void greetingSpeech(String nameRestaurant){
        System.out.println("<<Reception>> We are glad to greet you in " + nameRestaurant + " the best restaurant of our city");
    }

    default void farewellSpeech() {
        System.out.println("<<Reception>> Thank you for choosing a restaurant. All the best.");
    }
}
