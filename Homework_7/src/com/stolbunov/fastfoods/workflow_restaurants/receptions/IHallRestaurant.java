package com.stolbunov.fastfoods.workflow_restaurants.receptions;

import com.stolbunov.fastfoods.Client;

public interface IHallRestaurant {
    boolean acceptClient(Client client);

    Client getFirstClientToBeDischarged();
}
