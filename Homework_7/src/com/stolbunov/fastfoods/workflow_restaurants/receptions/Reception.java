package com.stolbunov.fastfoods.workflow_restaurants.receptions;

import com.stolbunov.fastfoods.Client;
import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.fastfoods.burgerclub.BurgerClub;
import com.stolbunov.fastfoods.mcdonalds.McDonald;
import com.stolbunov.fastfoods.workflow_restaurants.halls.hall_burger_club.HallBC;
import com.stolbunov.fastfoods.workflow_restaurants.halls.hall_mcdonalds.HallMD;

public class Reception implements IReception {
    private IHallRestaurant hallMD;
    private IHallRestaurant hallBC;

    public Reception() {
        hallMD = new HallMD();
        hallBC = new HallBC();

    }

    @Override
    public void addClientToHall(Client client, IRestaurant restaurant) {
        switch (restaurant.getName()) {
            case McDonald.RESTAURANT_NAME:
                placementClient(hallMD, client);
                break;
            case BurgerClub.RESTAURANT_NAME:
                placementClient(hallBC, client);
                break;
        }
    }

    private void searchFreeTable() {
        Client firstInQueue = hallMD.getFirstClientToBeDischarged();
        if (firstInQueue != null) {
            int time = firstInQueue.getTimeEating();
            messageNoVacantTables(time);
        }
    }

    private void placementClient(IHallRestaurant hallRestaurant, Client client) {
        if (!hallRestaurant.acceptClient(client)) {
            searchFreeTable();
        }
        messageFoundFreeTable();
    }

    private void messageNoVacantTables(int timeWaiting) {
        System.out.println("<<Reception>> Sorry but at the moment all the tables are busy! " +
                "\n<<Reception>> In " + timeWaiting + " seconds we will find you a free table");
    }

    private void messageFoundFreeTable() {
        System.out.println("<<Reception>> Thank you for your expectations. Your table is ready. Follow me.");
    }
}
