package com.stolbunov.fastfoods.workflow_restaurants.exception;

public class RestaurantException extends RuntimeException {
    public RestaurantException() {
        super();
    }

    public RestaurantException(String message) {
        super(message);
    }
}
