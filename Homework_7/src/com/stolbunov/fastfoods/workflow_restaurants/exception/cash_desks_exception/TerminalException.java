package com.stolbunov.fastfoods.workflow_restaurants.exception.cash_desks_exception;

import com.stolbunov.fastfoods.workflow_restaurants.exception.RestaurantException;

public class TerminalException extends RestaurantException {
    public TerminalException() {
        super();
    }

    public TerminalException(String message) {
        super(message);
    }
}
