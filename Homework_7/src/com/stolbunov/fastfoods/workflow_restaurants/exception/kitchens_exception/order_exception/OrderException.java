package com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception.order_exception;

import com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception.KitchenException;

public class OrderException extends KitchenException {
    public OrderException() {
        super();
    }

    public OrderException(String message) {
        super(message);
    }
}
