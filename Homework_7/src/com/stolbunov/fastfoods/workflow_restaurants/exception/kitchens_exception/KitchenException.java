package com.stolbunov.fastfoods.workflow_restaurants.exception.kitchens_exception;

import com.stolbunov.fastfoods.workflow_restaurants.exception.RestaurantException;

public class KitchenException extends RestaurantException {
    public KitchenException() {
        super();
    }

    public KitchenException(String message) {
        super(message);
    }
}
