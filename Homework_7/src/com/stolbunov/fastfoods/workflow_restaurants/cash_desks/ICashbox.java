package com.stolbunov.fastfoods.workflow_restaurants.cash_desks;

import com.stolbunov.fastfoods.Client;
import com.stolbunov.fastfoods.IRestaurant;

public interface ICashbox {
    void payment(Client client, IRestaurant restaurant);
}
