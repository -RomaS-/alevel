package com.stolbunov.fastfoods.workflow_restaurants.cash_desks;

public interface IMethodsPayment {
    void payment(int sumToPay);

    boolean isSufficientBalance(int sumToPay);
}
