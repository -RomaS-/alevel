package com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.cash_payments;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.BaseTypesPayment;

public class CashPaymentMD extends BaseTypesPayment {
    private static int balance;

    @Override
    public void addBalance(int money) {
        message("Cash...");
        balance += money;
    }

    @Override
    public int getBalance() {
        return balance;
    }
}
