package com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.ITypePayment;

public interface ICreateTypePayment {
    ITypePayment createMD();

    ITypePayment createBC();
}
