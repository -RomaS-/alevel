package com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.ITypePayment;

public abstract class BaseTypesPayment implements ITypePayment {

    protected void message(String typePayment) {
        System.out.println("<<Cashier>> Payment attempt by " + typePayment);
    }

    public void reportByBalance(int totalBalance, int cardBalance, int cashBalance) {
        System.out.println("<<Accountant>> The total balance is " + totalBalance + "$ of which according to the" +
                " Card: " + cardBalance + "$ and by Cash " + cashBalance + "$\n");
    }

}
