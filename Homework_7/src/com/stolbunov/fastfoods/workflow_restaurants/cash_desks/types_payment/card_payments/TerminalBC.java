package com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.card_payments;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.BaseTypesPayment;
import com.stolbunov.fastfoods.workflow_restaurants.exception.cash_desks_exception.TerminalException;

public class TerminalBC extends BaseTypesPayment {
    private static int balance;

    @Override
    public void addBalance(int money) throws TerminalException {
        message("Card...");
        if (dropTheCoin()) {
            throw new TerminalException(
                    "<<Cashier>> Sorry, but for technical reasons the terminal temporarily does not work.");
        }
        balance += money;
    }

    @Override
    public int getBalance() {
        return balance;
    }

    private boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }
}
