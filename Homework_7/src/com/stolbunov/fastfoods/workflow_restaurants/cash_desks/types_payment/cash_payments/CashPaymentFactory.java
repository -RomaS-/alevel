package com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.cash_payments;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.ITypePayment;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.ICreateTypePayment;

public class CashPaymentFactory implements ICreateTypePayment {
    @Override
    public ITypePayment createMD() {
        return new CashPaymentMD();
    }

    @Override
    public ITypePayment createBC() {
        return new CashPaymentBC();
    }
}
