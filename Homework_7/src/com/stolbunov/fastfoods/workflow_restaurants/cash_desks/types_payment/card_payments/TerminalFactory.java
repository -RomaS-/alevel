package com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.card_payments;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.ITypePayment;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.ICreateTypePayment;

public class TerminalFactory implements ICreateTypePayment {
    @Override
    public ITypePayment createMD() {
        return new TerminalMD();
    }

    @Override
    public ITypePayment createBC() {
        return new TerminalBC();
    }
}
