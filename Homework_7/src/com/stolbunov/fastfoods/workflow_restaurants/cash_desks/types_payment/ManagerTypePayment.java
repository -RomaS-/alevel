package com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment;

import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.fastfoods.burgerclub.BurgerClub;
import com.stolbunov.fastfoods.mcdonalds.McDonald;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.IChoiceTypePayment;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.IMethodsPayment;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.ITypePayment;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.card_payments.TerminalFactory;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.cash_payments.CashPaymentFactory;

public class ManagerTypePayment implements IChoiceTypePayment {
    private ICreateTypePayment cashPaymentFactory;
    private ICreateTypePayment terminalFactory;

    public ManagerTypePayment() {
        cashPaymentFactory = new CashPaymentFactory();
        terminalFactory = new TerminalFactory();
    }

    @Override
    public ITypePayment choice(IMethodsPayment payment, IRestaurant restaurant) {
        ICreateTypePayment iCreateTypePayment = choiceTypePayment(payment);
        return getPaymentForRestaurant(iCreateTypePayment, restaurant);
    }

    private ITypePayment getPaymentForRestaurant(ICreateTypePayment typePayment, IRestaurant restaurant) {
        switch (restaurant.getName()) {
            case McDonald.RESTAURANT_NAME:
                return typePayment.createMD();
            case BurgerClub.RESTAURANT_NAME:
                return typePayment.createBC();
        }
        throw new IllegalArgumentException("Does not match the name of the restaurant with the specified constants in CASE");
    }

    private ICreateTypePayment choiceTypePayment(IMethodsPayment payment) {
        if (payment instanceof ICard) {
            return terminalFactory;
        } else if (payment instanceof ICash) {
            return cashPaymentFactory;
        }
        throw new IllegalArgumentException("Unknown payment type! " +
                "The argument must implement one of the interfaces (ICard or ICash)");
    }
}
