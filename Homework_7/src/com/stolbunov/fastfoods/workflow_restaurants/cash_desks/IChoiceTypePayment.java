package com.stolbunov.fastfoods.workflow_restaurants.cash_desks;

import com.stolbunov.fastfoods.IRestaurant;

public interface IChoiceTypePayment {
    ITypePayment choice(IMethodsPayment payment, IRestaurant restaurant);
}
