package com.stolbunov.fastfoods.workflow_restaurants.cash_desks;

import com.stolbunov.fastfoods.Client;
import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.ManagerTypePayment;
import com.stolbunov.fastfoods.workflow_restaurants.exception.cash_desks_exception.TerminalException;
import com.stolbunov.fastfoods.workflow_restaurants.menu.Menu;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.IPackingOrder;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.kitchens.ICookFood;

import java.util.List;
import java.util.Map;

public class Cashbox implements ICashbox {
    private int sumToPayment;
    private IChoiceTypePayment choiceTypePayment;
    private Menu menu;

    public Cashbox() {
        menu = Menu.getInstance();
        choiceTypePayment = new ManagerTypePayment();
    }

    @Override
    public void payment(Client client, IRestaurant restaurant) {
        sumToPayment = 0;
        calculationToPayment(client, restaurant);
        messageAboutAmountToBePaid();
        transaction(client, restaurant);

    }

    private void transaction(Client client, IRestaurant restaurant) {
        List<IMethodsPayment> methodsPayment = client.getMethodsPayment();
        for (IMethodsPayment payment : methodsPayment) {
            try {
                transferMoney(payment, restaurant);
                break;
            } catch (TerminalException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void transferMoney(IMethodsPayment payment, IRestaurant restaurant) throws TerminalException {
        if (payment.isSufficientBalance(sumToPayment)) {
            ITypePayment choice = choiceTypePayment.choice(payment, restaurant);
            choice.addBalance(sumToPayment);
            payment.payment(sumToPayment);
            messageSuccessfulPayment();
        } else {
            messageNoMany();
        }
    }

    private void calculationToPayment(Client client, IRestaurant restaurant) {
        IPackingOrder finishedOrder = client.getFinishedOrder();
        List<ICookFood> kit = finishedOrder.getKit();
        Map<String, Integer> menu = this.menu.getMenu(restaurant);
        for (ICookFood food : kit) {
            sumToPayment += menu.get(food.getName());
        }
    }

    private void messageAboutAmountToBePaid() {
        System.out.println("<<Cashier>> The amount of your order is " + sumToPayment + "$.");
    }

    private void messageNoMany() {
        System.out.println("<<Cashier>> Sorry but you do not have sufficient means to pay..\n" +
                "<<Cashier>> To write on your account?\n" +
                "<<Client>> Yes. Many thanks!");
    }

    private void messageSuccessfulPayment() {
        System.out.println("<<Cashier>> Operation payment was successful!");
    }
}
