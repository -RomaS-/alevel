package com.stolbunov.fastfoods.workflow_restaurants.cash_desks;

import com.stolbunov.fastfoods.workflow_restaurants.exception.cash_desks_exception.TerminalException;

public interface ITypePayment {
    void addBalance(int money) throws TerminalException;

    int getBalance();

    void reportByBalance(int totalBalance, int cardBalance, int cashBalance);
}
