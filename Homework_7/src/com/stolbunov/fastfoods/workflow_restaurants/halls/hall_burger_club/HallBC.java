package com.stolbunov.fastfoods.workflow_restaurants.halls.hall_burger_club;

import com.stolbunov.fastfoods.Client;
import com.stolbunov.fastfoods.workflow_restaurants.receptions.IHallRestaurant;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class HallBC implements IHallRestaurant {

    private final int amountTables = 4;
    private Queue<Client> tablesForClients;

    public HallBC() {
        tablesForClients = new PriorityQueue<>(amountTables, Comparator.comparingInt(Client::getTimeEating));
    }

    @Override
    public boolean acceptClient(Client client) {
        if (tablesForClients.size() < amountTables) {
            return tablesForClients.offer(client);
        }
        return false;
    }

    @Override
    public Client getFirstClientToBeDischarged() {
        return tablesForClients.poll();
    }
}
