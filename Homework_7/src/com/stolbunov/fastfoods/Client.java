package com.stolbunov.fastfoods;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.IMethodsPayment;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.IPackingOrder;

import java.util.List;

public interface Client {
    float getMatch(IRestaurant IRestaurant);

    List<String> getOrders();

    int getTimeEating();

    void takeFinishedOrder(IPackingOrder order);

    IPackingOrder getFinishedOrder();

    List<IMethodsPayment> getMethodsPayment();

    void createRandomOrder(IRestaurant IRestaurant);
}
