package com.stolbunov.fastfoods.mcdonalds;

import com.stolbunov.fastfoods.*;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.Cashbox;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.ITypePayment;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.ICreateTypePayment;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.card_payments.TerminalFactory;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.cash_payments.CashPaymentFactory;
import com.stolbunov.fastfoods.workflow_restaurants.menu.Menu;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.ICashbox;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.IOrderTable;
import com.stolbunov.fastfoods.workflow_restaurants.receptions.IReception;
import com.stolbunov.fastfoods.workflow_restaurants.menu.IRestaurantMenu;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.TableOrder;
import com.stolbunov.fastfoods.workflow_restaurants.receptions.Reception;

import java.util.Collection;
import java.util.List;

public class McDonald implements IRestaurant {
    public static  final String RESTAURANT_NAME = "McDonald’s";
    private IOrderTable foodFactory;
    private IRestaurantMenu menu;
    private IReception reception;
    private ICashbox cashbox;
    private ICreateTypePayment terminalFactory;
    private ICreateTypePayment cashFactory;

    McDonald() {
        menu = Menu.getInstance();
        reception = new Reception();
        foodFactory = new TableOrder();
        cashbox = new Cashbox();
        terminalFactory = new TerminalFactory();
        cashFactory = new CashPaymentFactory();
    }

    @Override
    public String getName() {
        return RESTAURANT_NAME;
    }

    @Override
    public void feed(Client client) {
        reception.greetingSpeech(RESTAURANT_NAME);
        reception.addClientToHall(client, this);
        client.takeFinishedOrder(foodFactory.makeOrder(client.getOrders()));
        cashbox.payment(client, this);
        reception.farewellSpeech();
        createReportByBalance();
    }

    private void createReportByBalance() {
        ITypePayment terminal = terminalFactory.createMD();
        ITypePayment cash = cashFactory.createMD();
        int totalBalance = terminal.getBalance() + cash.getBalance();
        terminal.reportByBalance(totalBalance, terminal.getBalance(), cash.getBalance());
    }

    @Override
    public Collection<Integer> getPrice() {
        return menu.getPrice(this);
    }

    @Override
    public List<String> getAssortmentName() {
        return menu.getFullAssortment(this);
    }
}
