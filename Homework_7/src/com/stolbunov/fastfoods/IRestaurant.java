package com.stolbunov.fastfoods;

import java.util.Collection;
import java.util.List;

public interface IRestaurant {
    String getName();

    void feed(Client client);

    Collection<Integer> getPrice();

    List<String> getAssortmentName();


}