package com.stolbunov;

import com.stolbunov.fastfoods.Client;
import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.fastfoods.RestaurantFactory;
import com.stolbunov.students.Student;
import com.stolbunov.students.StudentsFactory;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final int STUDENTS_COUNT = 10;

    public static void main(String[] args) {
        StudentsFactory studentsFactory = new StudentsFactory();
        RestaurantFactory restaurantFactory = new RestaurantFactory();
        List<IRestaurant> restaurant = new ArrayList<>();
        restaurant.add(restaurantFactory.createRestaurant(RestaurantFactory.RestaurantType.BURGERCLUB));
        restaurant.add(restaurantFactory.createRestaurant(RestaurantFactory.RestaurantType.MCDONALDS));

        List<Student> students = new ArrayList<>(STUDENTS_COUNT);

        for (int i = 0; i < STUDENTS_COUNT; i++) {
            if (i % 2 == 0) {
                students.add(studentsFactory.createRichStudent());
            } else {
                students.add(studentsFactory.createSimpleStudent());
            }
        }

        for (Student student : students) {
            IRestaurant IRestaurant = chooseBestRestaurant(student, restaurant);
            student.createRandomOrder(IRestaurant);
            if (IRestaurant != null) {
                IRestaurant.feed(student);
            }
        }
    }

    private static IRestaurant chooseBestRestaurant(Client client, List<IRestaurant> restaurants) {
        float bestMatch = 0f;
        IRestaurant bestRestaurant = null;
        for (IRestaurant restaurant : restaurants) {
            float match = client.getMatch(restaurant);
            if (match > bestMatch){
                bestRestaurant = restaurant;
                bestMatch = match;
            }
        }
        System.out.println(bestRestaurant.getName());
        return bestRestaurant;
    }
}

