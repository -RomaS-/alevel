package com.stolbunov.students;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.IMethodsPayment;

import java.util.List;

public interface ICreationWallet {
    List<IMethodsPayment> create();
}
