package com.stolbunov.students.wallet.cards;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.types_payment.ICard;
import com.stolbunov.students.wallet.BaseMethods;

public class Card extends BaseMethods implements ICard {

    public Card(int balance) {
        super(balance);
    }
}
