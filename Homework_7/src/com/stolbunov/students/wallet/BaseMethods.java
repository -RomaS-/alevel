package com.stolbunov.students.wallet;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.IMethodsPayment;

public abstract class BaseMethods implements IMethodsPayment {
    protected int balance;

    public BaseMethods(int balance) {
        this.balance = balance;
    }

    @Override
    public void payment(int sumToPay) {
        balance -= sumToPay;
    }

    @Override
    public boolean isSufficientBalance(int sumToPay) {
        return balance >= sumToPay;
    }
}
