package com.stolbunov.students.wallet;

import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.IMethodsPayment;
import com.stolbunov.students.ICreationWallet;
import com.stolbunov.students.wallet.cards.Card;
import com.stolbunov.students.wallet.cash.Cash;

import java.util.ArrayList;
import java.util.List;

public class Wallet implements ICreationWallet {
    private List<IMethodsPayment> wallets;

    public Wallet() {
        wallets = new ArrayList<>();
        wallets.add(new Card(creationRandomBalance()));
        wallets.add(new Cash(creationRandomBalance()));
    }

    private int creationRandomBalance() {
        return (int) (Math.random() * 3) + 100;
    }

    @Override
    public List<IMethodsPayment> create() {
        return wallets;
    }
}
