package com.stolbunov.students;

import com.stolbunov.students.rich_students.RichStudent;
import com.stolbunov.students.simple_students.SimpleStudent;

public class StudentsFactory {
    public Student createSimpleStudent() {
        return new SimpleStudent();
    }

    public Student createRichStudent() {
        return new RichStudent();
    }
}


