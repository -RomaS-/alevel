package com.stolbunov.students.simple_students;

import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.students.Student;

public class SimpleStudent extends Student {


    @Override
    public float getMatch(IRestaurant IRestaurant) {
        if (super.getMatch(IRestaurant) > 100) {
            return 0;
        }
        return 1;
    }


}
