package com.stolbunov.students;

import com.stolbunov.fastfoods.Client;
import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.fastfoods.workflow_restaurants.cash_desks.IMethodsPayment;
import com.stolbunov.fastfoods.workflow_restaurants.table_order.IPackingOrder;
import com.stolbunov.students.wallet.Wallet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Student implements Client {
    private int speedEating;
    private List<String> orders;
    private IPackingOrder packingOrder;
    private ICreationWallet wallet;

    public Student() {
        speedEating = randomSpeedEating();
        orders = new ArrayList<>();
        wallet = new Wallet();
    }

    private int randomSpeedEating() {
        return (int) (Math.random() * 3) + 1;
    }

    @Override
    public void createRandomOrder(IRestaurant IRestaurant) {
        List<String> assortments = IRestaurant.getAssortmentName();
        byte[] randomNumbers = new byte[assortments.size()];
        Random random = new Random(System.currentTimeMillis());
        random.nextBytes(randomNumbers);
        for (int i = 0; i < assortments.size(); i++) {
            if (randomNumbers[i] % 2 == 0) {
                orders.add(assortments.get(i));
            }
        }
    }

    @Override
    public float getMatch(IRestaurant IRestaurant) {
        int sumPrice = 0;
        for (Integer price : IRestaurant.getPrice()) {
            sumPrice += price;
        }
        return sumPrice;
    }

    @Override
    public void takeFinishedOrder(IPackingOrder packingOrder) {
        this.packingOrder = packingOrder;
    }

    @Override
    public IPackingOrder getFinishedOrder() {
        return packingOrder;
    }

    @Override
    public List<IMethodsPayment> getMethodsPayment() {
        return wallet.create();
    }

    @Override
    public List<String> getOrders() {
        return orders;
    }

    @Override
    public int getTimeEating() {
        return speedEating;
    }
}
