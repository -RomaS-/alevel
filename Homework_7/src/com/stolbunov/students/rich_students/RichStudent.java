package com.stolbunov.students.rich_students;

import com.stolbunov.fastfoods.IRestaurant;
import com.stolbunov.students.Student;

public class RichStudent extends Student {
    @Override
    public float getMatch(IRestaurant IRestaurant) {
        if (super.getMatch(IRestaurant) < 100 && IRestaurant.getPrice().size() < 3) {
            return 0;
        }
        return 1;
    }
}
