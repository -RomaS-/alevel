package mosule1.starbucks;

import mosule1.consumers.IPaymentMethod;

public interface ICustomersStarbucks {
    int getNumberCoffee();

    String getName();

    void setCash(int newCash);

    int getCash();

    void setCard(int newBalance);

    int getCard();

    IPaymentMethod choicePayment();

    void setFlagPassagePayment(boolean isPassagePayment);

    boolean getFlagPassagePayment();
}
