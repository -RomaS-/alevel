package mosule1.starbucks.bar_counters.coffee.making_coffee;

import mosule1.starbucks.bar_counters.IOrderExecutors;
import mosule1.starbucks.bar_counters.coffee.kinds_coffe.coffee_pure.Americano;
import mosule1.starbucks.bar_counters.coffee.kinds_coffe.coffee_pure.EspressoClassic;
import mosule1.starbucks.bar_counters.coffee.kinds_coffe.coffee_with_additives.Latte;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.BreakageCoffeeMachineException;

public abstract class CreatorCoffee implements IOrderExecutors {
    private String nameCoffee = "";
    private IMixingIngredients[] setsCoffee;

    public CreatorCoffee() {
        setsCoffee = new IMixingIngredients[]{new Latte(), new EspressoClassic(), new Americano()};
    }

    public void createCoffee(int numberCoffee) throws BreakageCoffeeMachineException {
        createCoffeeByNumber(numberCoffee);
        printingInformationAboutWorkMachine();
    }

    private void createCoffeeByNumber(int numberCoffee) {
        switch (numberCoffee) {
            case LATTE:
                makeCoffee(getSetsCoffeeByIndex(LATTE));
                nameCoffee = "Latte";
                break;
            case ESPRESSO:
                makeCoffee(getSetsCoffeeByIndex(ESPRESSO));
                nameCoffee = "Espresso Classic";
                break;
            case AMERICANO:
                makeCoffee(getSetsCoffeeByIndex(AMERICANO));
                nameCoffee = "Americano";
                break;
        }
    }

    private void makeCoffee(IMixingIngredients kindsCoffee) {
        kindsCoffee.mixing();
    }

    private IMixingIngredients getSetsCoffeeByIndex(int index) {
        return setsCoffee[index - 1];
    }

    private void printingInformationAboutWorkMachine() {
        System.out.println("Order is ready <<" + nameCoffee + ">>");
    }
}
