package mosule1.starbucks.bar_counters.coffee.making_coffee.coffee_machines;

import mosule1.starbucks.bar_counters.coffee.making_coffee.CreatorCoffee;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.BreakageCoffeeMachineException;

public class CoffeeMachine extends CreatorCoffee {
    private boolean flagBreakage = false;

    private boolean generatorRandomBreakage() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }

    @Override
    public void createCoffee(int numberCoffee) throws BreakageCoffeeMachineException {
        flagBreakage = generatorRandomBreakage();
        if (flagBreakage) {
            throw new BreakageCoffeeMachineException("The coffee machine went out of order!");
        } else {
            super.createCoffee(numberCoffee);
        }
    }

    @Override
    public void repair() {
        flagBreakage = false;
    }
}
