package mosule1.starbucks.bar_counters.coffee.kinds_coffe;

import mosule1.starbucks.exception.coffee_exceptions.work_exception.NotEnoughResourcesException;

public interface IIngredientsForCoffee {
    int getIngredient(int amountPerServing) throws NotEnoughResourcesException;
}
