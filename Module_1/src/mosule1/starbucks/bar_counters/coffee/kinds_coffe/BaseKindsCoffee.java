package mosule1.starbucks.bar_counters.coffee.kinds_coffe;

import mosule1.starbucks.bar_counters.coffee.making_coffee.IMixingIngredients;
import mosule1.starbucks.bar_counters.coffee.resources_for_coffee.consumables.Cup;
import mosule1.starbucks.bar_counters.coffee.resources_for_coffee.ingredients.CoffeeBeans;
import mosule1.starbucks.bar_counters.coffee.resources_for_coffee.ingredients.Milk;
import mosule1.starbucks.bar_counters.coffee.resources_for_coffee.ingredients.Water;
import mosule1.starbucks.exception.coffee_exceptions.work_exception.NotEnoughResourcesException;

public abstract class BaseKindsCoffee implements IMixingIngredients {
    protected IConsumablesForCoffee cup;
    protected IIngredientsForCoffee water;
    protected IIngredientsForCoffee coffeeBeans;
    protected IIngredientsForCoffee milk;

    public BaseKindsCoffee() {
        cup = new Cup();
        water = new Water();
        coffeeBeans = new CoffeeBeans();
        milk = new Milk();
    }

    protected void getConsumablesForPortions(IConsumablesForCoffee consumables) throws NotEnoughResourcesException {
        consumables.getOneUnit();
    }

    protected void getIngredientForPortions(IIngredientsForCoffee ingredients, int amountPerServing) throws NotEnoughResourcesException {
        ingredients.getIngredient(amountPerServing);
    }
}
