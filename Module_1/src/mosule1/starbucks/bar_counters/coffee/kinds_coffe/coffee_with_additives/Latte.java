package mosule1.starbucks.bar_counters.coffee.kinds_coffe.coffee_with_additives;

import mosule1.starbucks.bar_counters.coffee.kinds_coffe.BaseKindsCoffee;

public class Latte extends BaseKindsCoffee {
    private final int COFFEE_PER_SERVING_G = 10;
    private final int WATER_PER_SERVING_ML = 35;
    private final int MILK_PER_SERVING_ML = 105;

    @Override
    public void mixing() {
        getConsumablesForPortions(cup);
        getIngredientForPortions(water, WATER_PER_SERVING_ML);
        getIngredientForPortions(coffeeBeans, COFFEE_PER_SERVING_G);
        getIngredientForPortions(milk, MILK_PER_SERVING_ML);
    }


}
