package mosule1.starbucks;

import mosule1.starbucks.bar_counters.BarCounter;
import mosule1.starbucks.bar_counters.coffee.resources_for_coffee.consumables.Cup;
import mosule1.starbucks.cashboxes.Cashbox;
import mosule1.starbucks.exception.cashbox_exceptions.CashboxException;

public class Starbucks {
    private int amountClients;
    private String[] textsOnCup;
    private int numberText = 0;
    private ICustomersStarbucks[] consumers;
    private IWorkWithBarCounters barCounters;
    private IWorkWithCashbox cashbox;

    public Starbucks(ICustomersStarbucks... consumers) {
        this.consumers = consumers;
        barCounters = new BarCounter();
        cashbox = new Cashbox();
        textsOnCup = textsForCup();
    }

    public void open() {
        for (ICustomersStarbucks consumer : consumers) {
            try {
                workProcess(consumer);
            } catch (CashboxException e) {
                System.out.println(e.getMessage() + "\n");
            }

        }
    }

    private void workProcess(ICustomersStarbucks consumer) {
        barCounters.completeOrder(consumer);
        cashbox.payment(consumer);
        countingClients();
        printOnCup(new Cup());
        System.out.println("Serviced " + amountClients + " customers\n");
    }

    private void countingClients() {
        amountClients++;
    }

    private void printOnCup(IPrintOnCup cup) {
        cup.print("The inscription on the cup: " + textsOnCup[numberText]);
        nextRecord();
        System.out.println(cup.getPrintOnCup());
    }

    private String[] textsForCup() {
        return new String[]{
                "<<<<I got up for a cup of coffee, and then did not notice how the day went. And so every morning ...>>>> ",
                "<<<<- What brought you closer? - She loved coffee, I - her>>>>",
                "<<<<Coffee is the favorite drink of the civilized world>>>>",
                "<<<<... do not deny yourself anything. What could be better than a cup of black coffee !?>>>>",
                "<<<<Drink coffee - and you can do stupid things even faster and more vigorously.>>>>",
                "<<<<Sleep is a symptom of caffeine starvation.>>>>"};
    }

    private void nextRecord() {
        if (numberText < textsOnCup.length - 1) {
            numberText++;
        } else {
            numberText = 0;
        }

    }
}
