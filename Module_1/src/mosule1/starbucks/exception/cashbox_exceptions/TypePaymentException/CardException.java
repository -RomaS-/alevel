package mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException;

import mosule1.starbucks.exception.cashbox_exceptions.CashboxException;

public class CardException extends CashboxException {
    public CardException() {
        super();
    }

    public CardException(String message) {
        super(message);
    }
}
