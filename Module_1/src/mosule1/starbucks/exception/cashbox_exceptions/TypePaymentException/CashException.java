package mosule1.starbucks.exception.cashbox_exceptions.TypePaymentException;

import mosule1.starbucks.exception.cashbox_exceptions.CashboxException;

public class CashException extends CashboxException {
    public CashException() {
        super();
    }

    public CashException(String message) {
        super(message);
    }
}
