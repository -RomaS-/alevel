package mosule1.starbucks.exception.cashbox_exceptions;

import mosule1.starbucks.exception.StarbucksException;

public class CashboxException extends StarbucksException {
    public CashboxException() {
        super();
    }

    public CashboxException(String message) {
        super(message);
    }
}
