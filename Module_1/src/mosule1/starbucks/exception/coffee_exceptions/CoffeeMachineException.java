package mosule1.starbucks.exception.coffee_exceptions;

import mosule1.starbucks.exception.StarbucksException;

public class CoffeeMachineException extends StarbucksException {
    public CoffeeMachineException() {
        super();
    }

    public CoffeeMachineException(String message) {
        super(message);
    }
}
