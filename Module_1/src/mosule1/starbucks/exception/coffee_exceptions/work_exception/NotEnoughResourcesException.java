package mosule1.starbucks.exception.coffee_exceptions.work_exception;

import mosule1.starbucks.bar_counters.coffee.kinds_coffe.IReplenishResources;
import mosule1.starbucks.exception.coffee_exceptions.CoffeeMachineException;

public class NotEnoughResourcesException extends CoffeeMachineException {
    private IReplenishResources resource;

    public NotEnoughResourcesException() {
        super();
    }

    public NotEnoughResourcesException(IReplenishResources resource, String message) {
        super(message);
        this.resource = resource;
    }

    public IReplenishResources getResource() {
        return resource;
    }
}
