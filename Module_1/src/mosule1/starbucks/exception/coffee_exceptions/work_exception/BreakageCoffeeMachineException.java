package mosule1.starbucks.exception.coffee_exceptions.work_exception;

import mosule1.starbucks.exception.coffee_exceptions.CoffeeMachineException;

public class BreakageCoffeeMachineException extends CoffeeMachineException {
    public BreakageCoffeeMachineException() {
        super();
    }

    public BreakageCoffeeMachineException(String message) {
        super(message);
    }
}
