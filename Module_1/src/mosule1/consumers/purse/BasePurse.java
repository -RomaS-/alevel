package mosule1.consumers.purse;

import mosule1.consumers.IPaymentMethod;

public abstract class BasePurse implements IPaymentMethod {
    private int balance;

    protected BasePurse(int balance) {
        this.balance = balance;
    }

    @Override
    public int getBalance() {
        return balance;
    }

    @Override
    public void setBalance(int newBalance) {
        balance = newBalance;
    }
}
